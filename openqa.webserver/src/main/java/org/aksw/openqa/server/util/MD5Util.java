package org.aksw.openqa.server.util;

import java.security.MessageDigest;

public class MD5Util {
	
	public static void main(String[] args) throws Exception {
		String login = "admin";
		System.out.println(MD5Util.encode(login));
	}
	
	public static String encode(String value) throws Exception {
		byte[] bytesOfMessage = value.getBytes("UTF-8");
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] thedigest = md.digest(bytesOfMessage);
		return new String(thedigest);
	}
}
