package org.aksw.openqa.server.view.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.aksw.openqa.server.util.MD5Util;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
@ManagedBean(name="loginSessionController")
@SessionScoped
public class LoginSessionController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9063953842583189546L;

	private static Logger logger = Logger.getLogger(ApplicationViewController.class);
	
	private String password;
	private String login;
	private boolean logged;
	
	@ManagedProperty(value = "#{applicationViewController}")
	private ApplicationViewController appViewConsole;
	
	public String login() {
		try {
			String encodedPass = MD5Util.encode(password);
			if(login != null && 
					login.equals(appViewConsole.getUser()) && 
					encodedPass != null  && 
							encodedPass.equals(appViewConsole.getPass())) {
				
				HttpSession session = (HttpSession) FacesContext.
		        	getCurrentInstance().
		        	getExternalContext().
		        	getSession(false);
				 
				session.setAttribute("logged", true);
				 
				return "/admin/manager.xhtml?faces-redirect=true";
			}
		} catch (Exception e) {
			logger.error("An error occurs while trying to login.");
		}
		
		FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Invalid Login!",
                    "Please Try Again!"));
		
		return "login";
	}
	
	public String logout() {
		 FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	     return "/index.xhtml?faces-redirect=true";
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean isLogged() {
		return logged;
	}
	
	public ApplicationViewController getAppViewConsole() {
		return appViewConsole;
	}
	
	public void setAppViewConsole(ApplicationViewController app) {
		appViewConsole = app;
	}
}
