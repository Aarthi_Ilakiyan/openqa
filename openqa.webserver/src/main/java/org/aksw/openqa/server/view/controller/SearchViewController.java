package org.aksw.openqa.server.view.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.aksw.openqa.Properties;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.providers.impl.RetrieverProvider;
import org.aksw.openqa.component.retriever.IResultEntryInstanciator;
import org.aksw.openqa.component.retriever.IResultEntryTriplestoreRetriever;
import org.aksw.openqa.component.retriever.PropertyEntry;
import org.aksw.openqa.component.retriever.ResultEntry;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.aksw.openqa.manager.plugin.PluginManagerPool;
import org.aksw.openqa.server.ServerEnviromentVariables;
import org.aksw.openqa.server.util.ServerSearchUtil;
import org.apache.log4j.Logger;
import org.json.JSONException;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
@ManagedBean(name="searchViewController")
@RequestScoped
public class SearchViewController implements Serializable {
	
	private static Logger logger = Logger.getLogger(SearchViewController.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = -7988952280420748804L;
	
	@ManagedProperty("#{param.q}")
	private String query;
	
	@ManagedProperty("#{param.content}")
	private String content;
	
	private Integer resultSize = 0;

	public SearchViewController() {
	}
	
	public String getQuery() {
		return query;
	}
	
	public void setQuery(String query) {
		this.query = query;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public int getResultSize() {
		return this.resultSize;
	}
	
	public List<ResourceEntry> getResult() {
		Map<String, Object> params = new HashMap<String, Object>();
		List<ResourceEntry> resources = new ArrayList<ResourceEntry>();
		
		// check if the query is pre-defined		
		if(query != null) {
			params.put("q", query);
		}
		if(content != null) {
			params.put("content", content);
		}
		
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().
				getExternalContext().getRequest();
		PluginManagerPool pool = (PluginManagerPool) ServerEnviromentVariables.getInstance().getParam(ServerEnviromentVariables.PLUGIN_MANAGER_POOL);
		PluginManager pluginManager = null;
		try {
			pluginManager = pool.borrowObject();
			IResultMapList<? extends IResultMap> result = ServerSearchUtil.search(params, request, pluginManager);
			resultSize = result.size();
			for(IResultMap resultEntry : result) {
				if(resultEntry.contains(Properties.URI)) {
					String resource = resultEntry.getParam(Properties.URI, String.class);
					ResourceEntry e = new URIEntry(resource, pluginManager, Integer.toString(result.indexOf(resultEntry)));
					resources.add(e);
				} else if(resultEntry.contains(Properties.SPARQL)) {
					String sparql = resultEntry.getParam(Properties.SPARQL, String.class);
					ResourceEntry e = new SPARQLEntry(sparql, Integer.toString(result.indexOf(resultEntry)));
					resources.add(e);
				}
			}
		} catch (Exception e1) {
			logger.error("Error searching.", e1);
		} finally {
			if(pluginManager != null) {
				pool.returnObject(pluginManager);
			}
		}
		return resources;
	}
	
	public class ResourceEntry implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -787982225724615754L;
		String id;
		
		public ResourceEntry(String id) {
			this.id = id;
		}
		
		public String getId() {
			return id;
		}
		
		public boolean isSparql() {
			return false;
		}
		
		public boolean isGeospatial() {
			return false;
		}
	}
	
	public class URIEntry extends ResourceEntry {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6402248767090179799L;
		String url;
		String json;
		String entryPath;
		Boolean isGeospatial = false;
		String lat;
		String lon;
		String label;
		String comment;
		String homepage;
		String imageURL;
		
		public URIEntry(String resource, PluginManager pluginManager, String id) {
			super(id);
			this.json = resource.replace("resource", "data") + ".json";
			this.url = resource;
			this.entryPath = resource.replace("http://dbpedia.org/resource/", "");
			try {
				load(url, pluginManager);
			} catch (Exception e) {
				logger.error("Error loadgin entity properties:" + url, e);
			}
		}

		@Override
		public boolean isGeospatial() {
			return isGeospatial;
		}
		
		private void load(String url, PluginManager pluginManager) throws IOException, JSONException {
			IResultEntryTriplestoreRetriever tripleStoreRetriever = pluginManager.getPlugin(RetrieverProvider.class, IResultEntryTriplestoreRetriever.class);
			String sparqlQuery =  "select ?p ?o where {?s ?p ?o. FILTER(?s = <" + url + ">). FILTER(!isLiteral(?o) || (langMatches(lang(?o), \"EN\"))) }";
			tripleStoreRetriever.retrieve(sparqlQuery, new IResultEntryInstanciator<Object>() {

				@Override
				public Object newInstance(ResultEntry entry) {
					String property = entry.getPropertyValue("p").toString();
					PropertyEntry o = entry.getProperty("o");

					if(property.equals("http://dbpedia.org/property/fullname")) {
						label = o.getValue().toString();
					} else if(property.equals("http://dbpedia.org/property/name")) {
						label = o.getValue().toString();
					} else if(property.equals("http://www.w3.org/2000/01/rdf-schema#label")) {
						label = o.getValue().toString();
					} else if(property.equals("http://www.w3.org/2003/01/geo/wgs84_pos#lat")) {
						isGeospatial = true;
						lat = o.getValue().toString();
					} else if(property.equals("http://www.w3.org/2003/01/geo/wgs84_pos#long")) {
						lon = o.getValue().toString();
					} else if(property.equals("http://www.w3.org/2000/01/rdf-schema#comment")) {
						comment = o.getValue().toString();
					} else if(property.equals("http://xmlns.com/foaf/0.1/depiction")) {
						imageURL = o.getValue().toString();
					} else	if(property.equals("http://xmlns.com/foaf/0.1/homepage")) {
						homepage = o.getValue().toString();
					}
					
					return null; // there is no need to return the Entry
				}

				@Override
				public boolean stop() {
					return false;
				}				
			});
		}
		
		public String getLat() {
			return lat;
		}
		
		public String getLon() {
			return lon;
		}
		
		public String getUrl() {
			return url;
		}
		
		public String getJson() {
			return json;
		}
		
		public String getEntryPath() {
			return entryPath;
		}
		
		public String getLabel() {
			return label;
		}
		
		public String getComment() {
			return comment;
		}
		
		public String getImageURL() {
			return imageURL;
		}
		
		public String getHomepage() {
			return homepage;
		}
		
//		private class EntityAttributes extends HashMap<String, String> {
//
//			/**
//			 * 
//			 */
//			private static final long serialVersionUID = -6975374213597324896L;
//			
//			String url;
//			public EntityAttributes(String url) {
//				this.url = url;
//			}
//			
//			public String getURL() {
//				return url;
//			}
//			
//			public void setUrl(String url) {
//				this.url = url;
//			}			
//		}
	}
	
	public class SPARQLEntry extends ResourceEntry {
		
		private static final long serialVersionUID = -1545984271398672497L;
		/**
		 * 
		 */
		String sparql;
		
		public SPARQLEntry(String content, String id) {
			super(id);
			this.sparql = content;
		}
		
		public String getContent() {
			return sparql;
		}
		
		@Override
		public boolean isSparql() {		
			return true;
		}
	}
}
