package org.aksw.openqa.component.answerformulation.queryparser.impl;

import java.net.MalformedURLException;
import java.net.URL;
import com.mysql.jdbc.Driver;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.aksw.autosparql.commons.index.Indices;
import org.aksw.autosparql.tbsl.algorithm.knowledgebase.RemoteKnowledgebase;
import org.aksw.autosparql.tbsl.algorithm.learning.TBSL;
import org.aksw.autosparql.tbsl.algorithm.learning.TemplateInstantiation;
import org.aksw.autosparql.tbsl.algorithm.search.BugfixedSolrIndex;
import org.aksw.openqa.Properties;
import org.aksw.openqa.commons.util.SPARQLUtil;
import org.aksw.openqa.component.answerformulation.AbstractQueryParser;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.ResultMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.service.cache.ICacheService;
import org.aksw.openqa.main.OpenQA;
import org.apache.log4j.Logger;
import org.dllearner.common.index.HierarchicalIndex;
import org.dllearner.common.index.Index;
import org.dllearner.kb.sparql.SparqlEndpoint;

public class TBSLQueryParser extends AbstractQueryParser {
	
	private static Logger logger = Logger.getLogger(TBSLQueryParser.class);
	
	//Component Parameters
	public final static String END_POINT_PARAM = "END_POINT_PARAM";
	public final static String SOLR_SERVER_PARAM = "SOLR_SERVER_PARAM";
	public final static String DEFAULT_GRAPH_PARAM = "DEFAULT_GRAPH_PARAM";
	
	//Cache Parameters
	public final static String CACHE_CONTEXT = "TBSL";
	//Defining Default Component Parameters
	{
		setParam(END_POINT_PARAM, "http://linkedspending.aksw.org/sparql");
		setParam(SOLR_SERVER_PARAM, "http://linkedspending.aksw.org/solr/en_");
		//setParam(SOLR_SERVER_PARAM, "http://solr.aksw.org/en_");
		setParam(DEFAULT_GRAPH_PARAM, "http://dbpedia.org");
	}
	
	private TBSL tbsl;
	private Driver driver;
	
	public TBSLQueryParser(Map<String, Object> params) {
		super(params);
		try {
			driver = new com.mysql.jdbc.Driver();
		} catch (SQLException e) {
			logger.warn("The com.mysql.jdbc.Driver could not be initialized.", e);
		}
	}
	
	@Override
	public boolean canProcess(IParamMap param) {
		return param.contains(Properties.Literal.TEXT);
	}

	@Override
	public List<? extends IResultMap> process(IParamMap param, 
			ServiceProvider serviceProvider, 
			IContext context) throws Exception {
		
		String query = (String) param.getParam(Properties.Literal.TEXT);		
		ICacheService cacheService = serviceProvider.get(ICacheService.class);		
		List<IResultMap> results = new ArrayList<IResultMap>();
		String sparql = cacheService.get(CACHE_CONTEXT, query, String.class);
		if(sparql == null) {
			TemplateInstantiation ti;
			ti = tbsl.answerQuestion(query);
			sparql = ti.getQuery();
			cacheService.put(CACHE_CONTEXT, query, sparql);
		}
		
		if(SPARQLUtil.isGeneric(sparql)) {
			logger.warn("TBSL generates a generic query."); 
			return results;
		}
		
		ResultMap r = new ResultMap();
		r.setParam(Properties.SPARQL, sparql);
				
		results.add(r);
		return results;
	}
	
	@Override
	public void setProperties(Map<String, Object> entries) {
		if(isActive()) {
			String knowledgeBase = (String) entries.get(END_POINT_PARAM);
			String defaultGraph = (String) entries.get(END_POINT_PARAM);
			String solrServer = (String) entries.get(SOLR_SERVER_PARAM);
			tbsl = newTBSLInstance(knowledgeBase, defaultGraph, solrServer);
		}
		super.setProperties(entries);
	}

	@Override
	public void startup() {
		try {
			java.sql.DriverManager.registerDriver(driver);
		} catch (SQLException e) {
			logger.warn("The driver could not be registered.", e);
		}		
		String knowledgeBase = (String) getParam(END_POINT_PARAM);
		String defaultGraph = (String) getParam(END_POINT_PARAM);
		String solrServer = (String) getParam(SOLR_SERVER_PARAM);
		tbsl = newTBSLInstance(knowledgeBase, defaultGraph, solrServer);
	}
	
	@Override
	public void shutdown() {
		tbsl = null;
		try {
			java.sql.DriverManager.deregisterDriver(driver);
		} catch (SQLException e) {
			logger.warn("The driver could not be deregistered.", e);
		}
	}
	
	private static TBSL newTBSLInstance(String knowledgeBase, String knowledgeGraph, String solrServer) {
		RemoteKnowledgebase rkbase = new RemoteKnowledgebase(newDbpediaEndpoint(knowledgeBase, knowledgeGraph), 
				"dbpedia", "DBpedia", newSolrIndices(solrServer));
		TBSL tbsl = new TBSL(rkbase, new String[]{"tbsl/lexicon/english.lex"});
		return tbsl;
	}
	
	private static SparqlEndpoint newDbpediaEndpoint(String endPointURL, String defautlGraph) {
		SparqlEndpoint endPoint = null;
		try{
			endPoint = new SparqlEndpoint(new URL(endPointURL), 
					Collections.<String>singletonList(defautlGraph), 
					Collections.<String>emptyList());
		}
		catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		return endPoint;
	}
	
	private static Indices newSolrIndices(String solrServerURL) {
		BugfixedSolrIndex resourcesIndex = new BugfixedSolrIndex(solrServerURL+"dbpedia_resources");		
		BugfixedSolrIndex classesIndex = new BugfixedSolrIndex(solrServerURL+"dbpedia_classes");
		BugfixedSolrIndex noBoaDataPropertiesIndex = new BugfixedSolrIndex(solrServerURL+"dbpedia_data_properties");
		BugfixedSolrIndex noBoaObjectPropertiesIndex = new BugfixedSolrIndex(solrServerURL+"dbpedia_data_properties");
		for(BugfixedSolrIndex index: new BugfixedSolrIndex[] {resourcesIndex, classesIndex, 
				noBoaObjectPropertiesIndex, noBoaDataPropertiesIndex})
		{index.setPrimarySearchField("label");}
		BugfixedSolrIndex boaIndex = new BugfixedSolrIndex(solrServerURL+"boa", "nlr-no-var");
		boaIndex.setSortField("boa-score");
		try {
			boaIndex.getResources("test");
		} catch (Error e) {
			logger.error("Error initializing the class.", e);
		} catch (Exception e) {
			logger.error("Error initializing the class.", e);
		}
		Index dataPropertiesIndex = new HierarchicalIndex(noBoaDataPropertiesIndex,boaIndex);
		Index objectPropertiesIndex = new HierarchicalIndex(noBoaObjectPropertiesIndex,boaIndex);
		Indices dbpediaIndices = new Indices(resourcesIndex, classesIndex, 
				objectPropertiesIndex, dataPropertiesIndex);
		return dbpediaIndices;
	}
		
	@Override
	public String getVersion() {
		return OpenQA.ENGINE_VERSION;
	}
	
	@Override
	public String getAPI() {
		return OpenQA.API_VERSION;
	}
}
