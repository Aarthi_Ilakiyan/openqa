package org.aksw.openqa.component.param;

public abstract class AbstractParamMap extends AbstractMap implements IParamMap {

	@Override
	public void accept(IMapVisitor visitor) {
		visitor.visit(this);
	}
}
