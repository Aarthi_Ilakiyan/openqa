package org.aksw.openqa.component.param;

public interface IMapConcrete {
	/**
	 * 
	 * @param visitor
	 */
	public void accept(IMapVisitor visitor);
}
