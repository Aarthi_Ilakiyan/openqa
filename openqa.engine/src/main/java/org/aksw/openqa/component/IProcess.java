package org.aksw.openqa.component;

import java.util.List;

import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.providers.impl.ServiceProvider;

public interface IProcess extends IComponent {
	IResultMapList<? extends IResultMap> process(List<? extends IParamMap> params, ServiceProvider services, IContext context) throws Exception;
}