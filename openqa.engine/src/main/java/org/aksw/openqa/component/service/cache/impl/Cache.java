package org.aksw.openqa.component.service.cache.impl;

import org.aksw.openqa.MaxSizeHashMap;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 * @param <K>
 * @param <V>
 */
public class Cache<K, V> {
	
	MaxSizeHashMap<K, V> objects;

	public Cache(int capacity) throws Exception {
		if(capacity < 1) {
			throw new Exception("Invalid capacity value:" + capacity);
		}
		this.objects = new MaxSizeHashMap<K, V>(capacity);
	}
	
	public synchronized void setCapacity(int capacity) {
		objects.setCapacity(capacity);
	}
	
	public synchronized boolean contains(K key) {
		return objects.containsKey(key);
	}
	
	public synchronized V get(K key) {
		return objects.get(key);
	}

	public synchronized void put(K key, V value) {
		objects.put(key, value);
	}
}
